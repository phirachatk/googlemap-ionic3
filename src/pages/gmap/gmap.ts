import { Component, ViewChild, ElementRef } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/*
 * AIzaSyAMr4KOo7wQbBsQo63O2nBmknyNlWz5hqA
 */

declare var google: any;

@IonicPage()
@Component({
  selector: 'page-gmap',
  templateUrl: 'gmap.html',
})
export class GmapPage {
  @ViewChild('map') mapRef: ElementRef;

  currentLocation: any;

  map: any;
  infoWindow: any;
  directionService = new google.maps.DirectionsService();
  directionDisplay = new google.maps.DirectionsRenderer();

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log(this.mapRef);
    this.calculateAndDisplayRoute();
  }

  calculateAndDisplayRoute() {
    let directionsService = new google.maps.DirectionsService;
    let directionsDisplay = new google.maps.DirectionsRenderer;

    const map = new google.maps.Map(this.mapRef.nativeElement, {
      zoom: 7,
      center: {lat: 0, lng: 0}
    });
    directionsDisplay.setMap(map);

    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition((position) => {
        console.log(position)
        const pos = {
        lat: position.coords.latitude,
        lng: position.coords.longitude
        };
        map.setCenter(pos);
        this.currentLocation = pos;
      });
    }

    directionsService.route({
      origin: this.currentLocation,
      destination: 'Central Plaza Ladprao',
      travelMode: 'DRIVING'
    }, function (response, status) {
      console.log(response)
      if (status === 'OK') {
        console.log(444)
        directionsDisplay.setDirections(response);
      } else {
        window.alert('Directions request failed due to ' + status);
      }
    });
  }
}
