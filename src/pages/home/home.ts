import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import * as mapboxgl from 'mapbox-gl';
// import { MapboxDirections } from '@mapbox/mapbox-gl-directions';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  lat = 0;
  lng = 0;

  constructor(public navCtrl: NavController) {

  }

  ionViewDidLoad() {

    // const directions = new MapboxDirections({
    //   accessToken: "pk.eyJ1Ijoiamlzb29uIiwiYSI6ImNqaTMydjNvZzBuMGgza3Fwc3RtYXB3MGcifQ.wADmXasttX9i4f4URe0cOw",
    //   unit: 'metric',
    //   profile: 'mapbox/driving'
    // })

    mapboxgl.accessToken = "pk.eyJ1Ijoiamlzb29uIiwiYSI6ImNqaTMydjNvZzBuMGgza3Fwc3RtYXB3MGcifQ.wADmXasttX9i4f4URe0cOw";
    navigator.geolocation.getCurrentPosition(position => {
      this.lng = position.coords.longitude;
      this.lat = position.coords.latitude;

      const map = new mapboxgl.Map({
        container: 'mapbox',
        style: 'mapbox://styles/mapbox/streets-v10',
        zoom: 11,
        center: [(this.lng + 100.571889) / 2, (this.lat + 13.849051) / 2]
      });

      // map.addControl(directions, 'top-left');
    });

    
  }

}


//https://api.mapbox.com/directions/v5/mapbox/cycling/-122.42,37.78;-77.03,38.91?access_token=
